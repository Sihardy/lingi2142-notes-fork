.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Traffic Control
===============

This chapter will describe the algorithms that can be used to control the flow of packets inside a network. Each algorithm will be described in details with a few illustrative examples and a set of multiple choice questions.

.. toctree::
   :maxdepth: 2

   Quality of Service parameters <qosparameters>
   Packet classification <classification>
   Policing and shaping <tokenbucket>
   Buffer Acceptance <bufferacceptance>
   Packet schedulers <schedulers>


