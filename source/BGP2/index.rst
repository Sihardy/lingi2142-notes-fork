.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Traffic engineering with Border Gateway Protocol
================================================

This chapter will describe traffic engineering techniques with the Border Gateway Protocol.