.. This document is part of the notes for the Computer Networks : Configuration and Management
.. course given at UCL

.. Use the sectionauthor directive to indicate the contributor to each section

Multicast routing protocols
===========================

This chapter will describe the operation of Multicast routing protocols.